const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: [true, 'User ID is required']
	},
	products: [
	 	{
			productId: {
				type: String,
				required: [false, 'Product ID is required']
			},
			quantity: {
				type: Number,
				required: [false, 'Quantity is required']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Description is required']
	},
	purchaseOn: {
		type: Date,
		default: new Date()
	}
	
	})
 
module.exports = mongoose.model("Order", orderSchema)