const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}, 
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	createDate: {
		type: Date,
		default: new Date()
	},
	userAddress: 
		{
			street1: String,
			street2: String,
			city: String,
			state: String,
			zip_code: Number,
			isPrimary: {
				type: Boolean,
				default: false
			}		
		}
	})


//use module.exports to export the model as a module, so that it can be imported in other files
module.exports = mongoose.model("User", userSchema)
