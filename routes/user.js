const express = require("express");
const router = express.Router(); //Create Route
const userController = require("../controllers/user")
const auth = require("../auth")


//Route for registration
router.post("/register", (req, res) => {
	console.log(req)
	userController.registerUser(req.body).then(resultFromController => res.send(
			resultFromController))
})


//Route to check for duplicate emails
router.post("/checkEmail", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(
			resultFromController))

})


// Route for login 
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController
		))
})





module.exports = router; 