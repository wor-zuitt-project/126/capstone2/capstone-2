const express = require("express");
const router = express.Router(); //Create Route
const orderController = require("../controllers/order")
const auth = require("../auth")



 

//Route for checkout

router.post("/", auth.verify, (req, res) => {
		
		//Now the token is good, so we can decode it and check whether it's admin
		// auth.decode turns out token into a decoded Javascript object 

		if (auth.decode(req.headers.authorization).isAdmin) {
			res.send("Admin cannot create order")
		} else {

			orderController.addOrder(req.body).then(resultFromController => res.send(
			resultFromController))

		}
})









// Retreive all Currently Logged in Order

router.get("/history", auth.verify, (req, res) => {
	const orderData = auth.decode(req.headers.authorization)

	orderController.getOrder(orderData).then(resultFromController => res.send(
		resultFromController))
})




// Admin Retreive all user in Order
router.get("/", auth.verify, (req, res) => {

	if (auth.decode(req.headers.authorization).isAdmin) {
		const retrieveData = auth.decode(req.headers.authorization)
		orderController.retrieveOrder(retrieveData).then(resultFromController => res.send(
			resultFromController))
	} else {
		res.send("Only admin can retrieve the order")
	}
})




module.exports = router; 




