const express = require('express')
const router = express.Router();
const productController = require("../controllers/product")
const auth = require("../auth")

//route to create a new produc 
// auth.verify will block all the code if the code hasn't pass the verification yet
router.post("/", auth.verify, (req, res) => {
		

		//Now the token is good, so we can decode it and check whether it's admin
		// auth.decode turns out token into a decoded Javascript object 

		if (auth.decode(req.headers.authorization).isAdmin) {
			productController.addProduct(req.body).then(resultFromController => res.send(
			resultFromController))
		} else { 
			res.send({auth: "failed"})

		}
 
	})


//route for getting a single product
router.get("/:productId", (req, res)=> {
	productController.getProduct(req.params).then(resultFromController => res.send 
				(resultFromController))
})



//route for getting all product with Active
router.get("/", (req, res) => {
	// console.log(req)
	productController.getProducts().then(resultFromController => res.send(
		resultFromController))
})




//update existing product
router.put("/:productId", auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
			productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)
				)
		} else {
			res.send({auth: "failed"})
		}	
})
 

//update existing product
router.put("/:productId", auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
			productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)
				)
		} else {
			res.send({auth: "failed"})


		}	
})



//update existing product
router.delete("/:productId", (req, res) => {

if (auth.decode(req.headers.authorization).isAdmin) {
			productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController)
				)
		} else {
			res.send({auth: "failed"})

		}	

})









module.exports = router; 

		