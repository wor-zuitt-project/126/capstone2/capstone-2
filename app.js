// import dependencies 
const express = require("express")
const mongoose = require("mongoose")


//import routes
const userRoutes = require("./routes/user") //allows us to use the content of our user routes file
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")


 


//add database connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.uhxum.mongodb.net/ecommerce_api?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


 

// Confirm Connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))




//server setup

const app = express()

app.use(express.json())

app.use(express.urlencoded({
	extended: true
}))



//add routes
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)


const port = 4000

//listent to the server 
app.listen(process.envPORT || port, () => {
	console.log(`Server running at port ${port}`)
})