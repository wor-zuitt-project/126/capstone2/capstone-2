const Product = require("../models/product");


//Create a new product
module.exports.addProduct = (body) => {
	let newProduct = new Product ({
		name: body.name,
		description: body.description,
		price: body.price
		// createOn: body.createOn
		// 10 refers to a concept called "salt" which is the number of 
		// times encryption is run on the password
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false // Product was not save
		} else {
			return true //Successfully save
		}
	})
} 


//get specfic product
module.exports.getProduct = (params) => {
	//findById is a Mongoose operation that just finds a document by its ID
	return Product.findById(params.productId).then(result => {
		return result;
	})
}



//get All Product with Active 
module.exports.getProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result; //Find all product, then return result
	})
}

//update specific product
//Why 2 things. Because 1) What product we want to update? -> Param
// 2) How we update? -> body
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((
		product, err) => {

		//error handling
		if(err){
			return false;
		} else {
			return true;
		}
	})
}



//archive Product (Active to Inactive)

module.exports.archiveProduct = (params, body) => {

	let archiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, archiveProduct).then((
		product, err) => {

		//error handling
		if(err){
			return false;
		} else {
			return true;
		}
	})


}

