//import dependencies
const User = require("../models/user.js")
const bcrypt = require("bcrypt") // to encrypt our passwords
const auth = require("../auth") //import auth.js file to use its authorization functions


// Check Email
module.exports.checkEmail = (body) => {
	return User.find({email: body.email}).then(result => {
	if(result.length > 0){
			return true; //duplicate email found
		} else {
			return false; //email not yet registered
		}
	})
}



// Register User
module.exports.registerUser = (body) => {
	let newUser = new User ({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		mobileNo: body.mobileNo,
		password: bcrypt.hashSync(body.password, 10)
		// 10 refers to a concept called "salt" which is the number of 
		// times encryption is run on the password
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false // User was not save
		} else {
			return true //Successfully save
		}
	})
}


// Login User 
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false //user does not exist
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
		//compareSync() is used to compare a non-encrypted password to the encrypted password
		// in our database. If there is a match, it returns true. Otherwise, it returns false.
			if(isPasswordCorrect){
					return {accessToken: auth.createAccessToken(result.toObject())}
					// return an object with 1 property which is access token
			} else {
					return false; 
			}
		}
	})
}

